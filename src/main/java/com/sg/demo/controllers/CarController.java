package com.sg.demo.controllers;

import com.sg.demo.dto.CarDto;
import com.sg.demo.dto.CarFilterQriteria;
import com.sg.demo.services.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cars")
public class CarController {

    @Autowired
    private CarService carService;

    @GetMapping
    public List<CarDto> index(CarFilterQriteria filters) {
        return carService.findAll(filters);
    }

    @PostMapping
    public CarDto create(@RequestBody CarDto carDto) {
        return carService.create(carDto);
    }

    @GetMapping("/{id}")
    public CarDto show(@PathVariable(name = "id") Long id) {
        return carService.findById(id);
    }

    @PutMapping("/{id}")
    public CarDto update(@PathVariable(name = "id") Long id, @RequestBody CarDto carDto) {
        CarDto existing = carService.findById(id);
        if (carDto.getCarModelId() != null) existing.setCarModelId(carDto.getCarModelId());
        return carService.update(existing);
    }

    @DeleteMapping("/{id}")
    public CarDto destroy(@PathVariable(name = "id") Long id) {
        CarDto carDto = carService.findById(id);
        return carService.destroy(carDto);
    }

}
