package com.sg.demo.controllers;

import com.sg.demo.dto.CarMakeDto;
import com.sg.demo.services.CarMakeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/car-makes")
public class CarMakeController {

    @Autowired
    private CarMakeService carMakeService;

    @GetMapping
    public List<CarMakeDto> index() {
        return carMakeService.findAll();
    }

    @PostMapping
    public CarMakeDto create(@RequestBody CarMakeDto carMakeDto) {
        return carMakeService.create(carMakeDto);
    }

    @GetMapping("/{id}")
    public CarMakeDto show(@PathVariable(name = "id") Long id) {
        return carMakeService.findById(id);
    }

    @PutMapping("/{id}")
    public CarMakeDto update(@PathVariable(name = "id") Long id, @RequestBody CarMakeDto carMakeDto) {
        CarMakeDto existing = carMakeService.findById(id);
        if (carMakeDto.getName() != null) existing.setName(carMakeDto.getName());
        return carMakeService.update(existing);
    }

    @DeleteMapping("/{id}")
    public CarMakeDto destroy(@PathVariable(name = "id") Long id) {
        CarMakeDto carMakeDto = carMakeService.findById(id);
        return carMakeService.destroy(carMakeDto);
    }

}
