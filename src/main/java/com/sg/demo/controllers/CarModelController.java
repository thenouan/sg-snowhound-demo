package com.sg.demo.controllers;

import com.sg.demo.dto.CarModelDto;
import com.sg.demo.services.CarModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/car-models")
public class CarModelController {

    @Autowired
    private CarModelService carModelService;

    @GetMapping
    public List<CarModelDto> index() {
        return carModelService.findAll();
    }

    @PostMapping
    public CarModelDto create(@RequestBody CarModelDto carModelDto) {
        return carModelService.create(carModelDto);
    }

    @GetMapping("/{id}")
    public CarModelDto show(@PathVariable(name = "id") Long id) {
        return carModelService.findById(id);
    }

    @PutMapping("/{id}")
    public CarModelDto update(@PathVariable(name = "id") Long id, @RequestBody CarModelDto carModelDto) {
        CarModelDto existing = carModelService.findById(id);
        if (carModelDto.getName() != null) existing.setName(carModelDto.getName());
        return carModelService.update(existing);
    }

    @DeleteMapping("/{id}")
    public CarModelDto destroy(@PathVariable(name = "id") Long id) {
        CarModelDto carModelDto = carModelService.findById(id);
        return carModelService.destroy(carModelDto);
    }

}
