package com.sg.demo.controllers;

import com.sg.demo.dto.BookingDto;
import com.sg.demo.services.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/bookings")
public class BookingController {

    @Autowired
    private BookingService bookingService;

    @GetMapping
    public List<BookingDto> index() {
        return bookingService.findAll();
    }

    @PostMapping
    public BookingDto create(@RequestBody BookingDto bookingDto, Principal principal) {
        bookingDto.setUserId(principal.getName());
        return bookingService.create(bookingDto);
    }

    @GetMapping("/{id}")
    public BookingDto show(@PathVariable(name = "id") Long id) {
        return bookingService.findById(id);
    }

    @PutMapping("/{id}")
    public BookingDto update(@PathVariable(name = "id") Long id, @RequestBody BookingDto bookingDto, Principal principal) {
        BookingDto existing = bookingService.findById(id);
        if (bookingDto.getCarId() != null) existing.setCarId(bookingDto.getCarId());
        if (bookingDto.getStatus() != null) existing.setStatus(bookingDto.getStatus());
        if (bookingDto.getUserId() != null) {
            existing.setUserId(bookingDto.getUserId());
        } else {
            existing.setUserId(principal.getName());
        };
        return bookingService.update(existing);
    }

    @DeleteMapping("/{id}")
    public BookingDto destroy(@PathVariable(name = "id") Long id) {
        BookingDto bookingDto = bookingService.findById(id);
        return bookingService.destroy(bookingDto);
    }
}