package com.sg.demo.repositories;

import com.sg.demo.models.Car;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import java.util.ArrayList;

public class CarSpecifications {
    public enum SortableFields {
        booked,
        makeModelName,
    }

    public enum SortDirections {
        ASC,
        DESC,
    }

    public static Specification<Car> withBooked(Boolean booked) {
        if (booked == null) return null;
        return (root, query, cb) -> cb.equal(root.get("booked"), booked);
    }

    public static Specification<Car> withOrder(SortableFields field, SortDirections direction) {
        return (root, query, cb) -> {
            ArrayList<Path<Object>> paths = new ArrayList<>();

            switch (field) {
                case booked:
                    paths.add(root.get("booked"));
                case makeModelName:
                    paths.add(root.join("carModel").get("carMake").get("name"));
                    paths.add(root.join("carModel").get("name"));
            }

            Order[] orders = null;

            switch (direction) {
                case ASC -> orders = paths.stream().map(cb::asc).toArray(Order[]::new);
                case DESC -> orders = paths.stream().map(cb::desc).toArray(Order[]::new);
            }

            query.orderBy(orders);

            return null;
        };
    }
}
