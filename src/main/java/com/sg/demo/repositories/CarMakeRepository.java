package com.sg.demo.repositories;

import com.sg.demo.models.CarMake;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarMakeRepository extends JpaRepository<CarMake, Long> {
}
