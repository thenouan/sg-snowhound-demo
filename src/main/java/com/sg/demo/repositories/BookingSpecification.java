package com.sg.demo.repositories;

import com.sg.demo.models.Booking;
import org.springframework.data.jpa.domain.Specification;

public class BookingSpecification {
    public static Specification<Booking> withCarId(Long carId) {
        return (root, query, cb) -> cb.equal(root.get("car"), carId);
    }

    public static Specification<Booking> withStatus(String status) {
        return (root, query, cb) -> cb.equal(root.get("status"), status);
    }
}