package com.sg.demo.services;

import com.sg.demo.dto.CarModelDto;
import com.sg.demo.models.CarModel;
import com.sg.demo.repositories.CarModelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CarModelService {

    @Autowired
    private CarModelRepository carModelRepository;

    @Autowired
    private CarMakeService carMakeService;

    public List<CarModelDto> findAll() {
        return carModelRepository.findAll().stream().map(this::convertEntityToDto).collect(Collectors.toList());
    }

    public CarModelDto findById(long id) {
        Optional<CarModel> carModel = carModelRepository.findById(id);
        if (carModel.isEmpty()) return null;

        return convertEntityToDto(carModel.get());
    }

    @PreAuthorize("hasRole('admin')")
    public CarModelDto create(CarModelDto input) {
        CarModel saved = carModelRepository.saveAndFlush(convertDtoToEntity(input));
        return findById(saved.getId());
    }

    @PreAuthorize("hasRole('admin')")
    public CarModelDto update(CarModelDto input) {
        Optional<CarModel> carModelOptional = carModelRepository.findById(input.getId());
        if (carModelOptional.isEmpty()) return null;
        CarModel carModel = carModelOptional.get();

        carModel.setName(input.getName());

        return convertEntityToDto(carModelRepository.save(carModel));
    }

    @PreAuthorize("hasRole('admin')")
    public CarModelDto destroy(CarModelDto carModelDto) {
        carModelRepository.delete(convertDtoToEntity(carModelDto));
        return carModelDto;
    }

    protected CarModel convertDtoToEntity(CarModelDto dto) {
        if (dto == null) return null;
        CarModel carModel = new CarModel();
        carModel.setId(dto.getId());
        carModel.setCarMake(carMakeService.convertDtoToEntity(carMakeService.findById(dto.getCarMakeId())));
        carModel.setName(dto.getName());
        return carModel;
    }

    protected CarModelDto convertEntityToDto(CarModel entity) {
        if (entity == null) return null;
        CarModelDto carModelDto = new CarModelDto();
        carModelDto.setId(entity.getId());
        carModelDto.setName(entity.getName());
        carModelDto.setCarMakeId(entity.getCarMake().getId());
        carModelDto.setCarMake(carMakeService.convertEntityToDto(entity.getCarMake()));
        return carModelDto;
    }
}
