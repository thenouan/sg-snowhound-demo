package com.sg.demo.services;

import com.sg.demo.dto.CarDto;
import com.sg.demo.models.Car;
import com.sg.demo.repositories.CarRepository;
import com.sg.demo.repositories.CarSpecifications;
import com.sg.demo.dto.CarFilterQriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CarService {

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private CarModelService carModelService;

    @Autowired
    private BookingService bookingService;

    public List<CarDto> findAll() {
        return findAll(null);
    }

    public List<CarDto> findAll(CarFilterQriteria qriteria) {
        List<Car> resultSet;

        if (qriteria != null) {
            resultSet = carRepository.findAll(
                    Specification
                            .where(CarSpecifications.withOrder(CarSpecifications.SortableFields.booked, CarSpecifications.SortDirections.ASC))
                            .and(CarSpecifications.withBooked(qriteria.getBooked()))
            );
        } else {
            resultSet = carRepository.findAll();
        }

        return resultSet.stream().map(this::convertEntityToDto).collect(Collectors.toList());
    }

    public CarDto findById(long id) {
        Optional<Car> car = carRepository.findById(id);
        if (car.isEmpty()) return null;

        return convertEntityToDto(car.get());
    }

    @PreAuthorize("hasRole('admin')")
    public CarDto create(CarDto input) {
        return convertEntityToDto(carRepository.save(convertDtoToEntity(input)));
    }

    @PreAuthorize("hasRole('admin')")
    public CarDto update(CarDto input) {
        Optional<Car> carOptional = carRepository.findById(input.getId());
        if (carOptional.isEmpty()) return null;
        Car car = carOptional.get();

        car.setCarModel(carModelService.convertDtoToEntity(carModelService.findById(input.getCarModelId())));
        car.setBooked(input.getBooked());

        return convertEntityToDto(carRepository.save(car));
    }

    @PreAuthorize("hasRole('admin') and !@bookingService.hasBooking(#carDto)")
    public CarDto destroy(CarDto carDto) {
        carRepository.delete(convertDtoToEntity(carDto));
        return carDto;
    }

    protected Car convertDtoToEntity(CarDto dto) {
        if (dto == null) return null;
        Car car = new Car();
        car.setId(dto.getId());
        car.setCarModel(carModelService.convertDtoToEntity(carModelService.findById(dto.getCarModelId())));
        if (dto.getBooked() != null) {
            car.setBooked(dto.getBooked());
        } else {
            car.setBooked(false);
        }
        return car;
    }

    protected CarDto convertEntityToDto(Car entity) {
        if (entity == null) return null;
        CarDto carDto = new CarDto();
        carDto.setId(entity.getId());
        carDto.setCarModelId(entity.getCarModel().getId());
        carDto.setCarModel(carModelService.convertEntityToDto(entity.getCarModel()));
        carDto.setBooked(entity.getBooked());
        return carDto;
    }
}
