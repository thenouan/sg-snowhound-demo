package com.sg.demo.services;

import com.sg.demo.dto.CarMakeDto;
import com.sg.demo.models.CarMake;
import com.sg.demo.repositories.CarMakeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CarMakeService {

    @Autowired
    private CarMakeRepository carMakeRepository;

    public List<CarMakeDto> findAll() {
        return carMakeRepository.findAll().stream().map(this::convertEntityToDto).collect(Collectors.toList());
    }

    public CarMakeDto findById(long id) {
        Optional<CarMake> carMake = carMakeRepository.findById(id);
        if (carMake.isEmpty()) return null;

        return convertEntityToDto(carMake.get());
    }

    @PreAuthorize("hasRole('admin')")
    public CarMakeDto create(CarMakeDto input) {
        CarMake saved = carMakeRepository.save(convertDtoToEntity(input));
        return convertEntityToDto(carMakeRepository.getById(saved.getId()));
    }

    @PreAuthorize("hasRole('admin')")
    public CarMakeDto update(CarMakeDto input) {
        Optional<CarMake> carMakeOptional = carMakeRepository.findById(input.getId());
        if (carMakeOptional.isEmpty()) return null;
        CarMake carMake = carMakeOptional.get();

        carMake.setName(input.getName());

        return convertEntityToDto(carMakeRepository.save(carMake));
    }

    @PreAuthorize("hasRole('admin')")
    public CarMakeDto destroy(CarMakeDto carMakeDto) {
        carMakeRepository.delete(convertDtoToEntity(carMakeDto));
        return carMakeDto;
    }

    protected CarMake convertDtoToEntity(CarMakeDto dto) {
        if (dto == null) return null;
        CarMake carMake = new CarMake();
        carMake.setId(dto.getId());
        carMake.setName(dto.getName());
        return carMake;
    }

    protected CarMakeDto convertEntityToDto(CarMake entity) {
        if (entity == null) return null;
        CarMakeDto carMakeDto = new CarMakeDto();
        carMakeDto.setId(entity.getId());
        carMakeDto.setName(entity.getName());
        return carMakeDto;
    }
}
