package com.sg.demo.services;

import com.sg.demo.dto.BookingDto;
import com.sg.demo.dto.CarDto;
import com.sg.demo.models.Booking;
import com.sg.demo.repositories.BookingRepository;
import com.sg.demo.repositories.BookingSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class BookingService {

    @Autowired
    private CarService carService;

    @Autowired
    private BookingRepository bookingRepository;

    public boolean hasBooking(CarDto carDto) {
        return this.hasBookingByCarId(carDto.getId());
    }

    public boolean hasBookingByCarId(Long carId) {
        Optional<Booking> activeBookingOptional = bookingRepository.findOne(
                Specification.where(BookingSpecification.withCarId(carId))
                        .and(BookingSpecification.withStatus("active"))
        );
        return activeBookingOptional.isPresent();
    }

    public List<BookingDto> findAll() {
        return bookingRepository.findAll().stream().map(this::convertEntityToDto).collect(Collectors.toList());
    }

    public BookingDto findById(long id) {
        Optional<Booking> bookingOptional = bookingRepository.findById(id);
        if (bookingOptional.isEmpty()) return null;

        return convertEntityToDto(bookingOptional.get());
    }

    @PreAuthorize("!this.hasBookingByCarId(#input.getCarId())")
    public BookingDto create(BookingDto input) {
        Booking booking = bookingRepository.save(convertDtoToEntity(input));

        CarDto carDto = carService.findById(input.getCarId());
        carDto.setBooked(true);
        carService.update(carDto);

        return convertEntityToDto(booking);
    }

    @PreAuthorize("hasRole('admin') or #input.getUserId().equals(authentication.principal.getName())")
    public BookingDto update(BookingDto input) {
        Optional<Booking> bookingOptional = bookingRepository.findById(input.getId());
        if (bookingOptional.isEmpty()) return null;
        Booking booking = bookingOptional.get();

        booking.setCar(carService.convertDtoToEntity(carService.findById(input.getCarId())));
        booking.setUserId(input.getUserId());
        booking.setStatus(input.getStatus().toString());


        CarDto carDto = carService.findById(input.getCarId());
        carDto.setBooked(input.getStatus() == BookingDto.BookingStatus.active);
        carService.update(carDto);

        booking = bookingRepository.save(booking);

        return convertEntityToDto(booking);
    }

    @PreAuthorize("hasRole('admin')")
    public BookingDto destroy(BookingDto input) {
        bookingRepository.delete(convertDtoToEntity(input));
        return input;
    }

    protected Booking convertDtoToEntity(BookingDto dto) {
        if (dto == null) return null;
        Booking booking = new Booking();
        booking.setId(dto.getId());
        booking.setCar(carService.convertDtoToEntity(carService.findById(dto.getCarId())));
        booking.setUserId(dto.getUserId());
        booking.setStatus(dto.getStatus().toString());
        return booking;
    }

    protected BookingDto convertEntityToDto(Booking entity) {
        if (entity == null) return null;
        BookingDto bookingDto = new BookingDto();
        bookingDto.setId(entity.getId());
        bookingDto.setCarId(entity.getCar().getId());
        bookingDto.setCar(carService.convertEntityToDto(entity.getCar()));
        bookingDto.setUserId(entity.getUserId());
        bookingDto.setStatus(BookingDto.BookingStatus.valueOf(entity.getStatus()));
        return bookingDto;
    }
}
