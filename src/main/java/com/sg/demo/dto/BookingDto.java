package com.sg.demo.dto;

import lombok.Data;

@Data
public class BookingDto {
    public enum BookingStatus {
        active,
        finished,
    }

    private Long id;
    private Long carId;
    private CarDto car;
    private String userId;
    private BookingStatus status;
}
