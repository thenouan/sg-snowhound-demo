package com.sg.demo.dto;

import lombok.Data;

@Data
public class CarFilterQriteria {
    enum SortOrder {
        asc,
        desc,
    }

    @Data
    static class Sort {
        private SortOrder booked;
        private SortOrder makeModelName;

    }

    private Boolean booked;
    private Sort sort;
}

