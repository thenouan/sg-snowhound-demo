package com.sg.demo.dto;

import lombok.Data;

@Data
public class CarMakeDto {
    private Long id;
    private String name;
}
