package com.sg.demo.dto;

import lombok.Data;

@Data
public class CarModelDto {
    private Long id;
    private String name;
    private Long carMakeId;
    private CarMakeDto carMake;
}
