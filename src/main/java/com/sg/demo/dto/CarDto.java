package com.sg.demo.dto;

import com.sg.demo.models.Booking;
import lombok.Data;

import java.util.List;

@Data
public class CarDto {
    private Long id;
    private Long carModelId;
    private CarModelDto carModel;
    private Boolean booked;
    private List<BookingDto> bookings;
}
