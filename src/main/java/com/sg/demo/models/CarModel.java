package com.sg.demo.models;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Data
public class CarModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "car_make_id")
    private CarMake carMake;

    @NotNull
    private String name;

    @OneToMany(mappedBy="carModel")
    private Set<Car> cars;
}
