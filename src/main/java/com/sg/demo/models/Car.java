package com.sg.demo.models;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Data
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "car_model_id")
    private CarModel carModel;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "car_id")
    private List<Booking> booking;

    private Boolean booked = false;
}
