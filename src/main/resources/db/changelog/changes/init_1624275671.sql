create table "car_make" (
                             id bigserial not null,
                             name varchar(255) not null,
                             primary key (id)
);

create table "car_model" (
                              id bigserial not null,
                              car_make_id bigserial not null,
                              name varchar(255) not null,
                              primary key (id),
                              foreign key (car_make_id) references car_make(id) on delete restrict
);

create table "car" (
                        id bigserial not null,
                        car_model_id bigserial not null,
                        booked boolean default false,
                        primary key (id),
                        foreign key (car_model_id) references car_model(id) on delete restrict
);

create table "booking" (
                       id bigserial not null,
                       car_id bigserial not null,
                       user_id varchar(255) not null,
                       status varchar(255) default 'active' not null,
                       primary key (id),
                       foreign key (car_id) references car(id) on delete restrict
);
